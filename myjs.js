/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function dataValidate()
{
    var name = document.getElementById("txtName");
    var mobile = document.getElementById("txtMobile");
    var email = document.getElementById("txtEmail");
    var comments = document.getElementById("txaComments");
	var plate = document.getElementById("txtPlate");
	var model;

    for (index=0; index < document.form1.model.length; index++) 
    {
        if (document.form1.model[index].checked) 
        {
            model = "1";
            break;
        }
        else
        {
            model = "";
        }
    }
  
    if(name.value=="" && mobile.value=="" && email.value=="" && comments.value=="" && model =="")
    {
        alert("Please fill in all fields!");
    }
    else
    {
	if(name.value=="")
        {
            alert("Please fill in your name!");
        }
        else if(mobile.value=="")
        {
            alert("Please fill in your mobile number!");
        }
        else if(email.value=="")
        {
            alert("Please fill in your email address!");
        }
        else if(comments.value=="")
        {
            alert("Please fill in your comments!");
        }
//		else if(plate.value=="")
//        {
//            alert("Please fill in your plate number!");
//        }
		else if(model =="")
        {
            alert("Please choose a model!");
        }
        else
        {
            document.form1.action = "sendmail.php";
            document.form1.submit();
        }
    }
}

function valEmail()
{
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var address = document.getElementById("txtEmail").value;
    var status = new Boolean();
    if(reg.test(address) == false) 
    {
      alert('Invalid Email Address');
      document.getElementById("txtEmail").value="";
      document.getElementById("txtEmail").style.background="#ff7e0e";
      status = false;
    }
    else
    {
        document.getElementById("txtEmail").style.background="#414141";
        status = true;
    }
    return status;
}

function valMobile()
{
    var phone = document.getElementById("txtMobile").value;
    var phoneRegex = /^\d\d\d\d\d\d\d\d\d\d$/;
    var status = new Boolean();
    if(!phone.match( phoneRegex )) 
    {
        alert("Invalid phone number!");
        document.getElementById("txtMobile").value="";
        document.getElementById("txtMobile").style.background="#ff7e0e";
        status = false;
    }
    else
    {
        document.getElementById("txtMobile").style.background="#414141";
        status = true;
    }
    return status;
}

function valName()
{
    var name = document.getElementById("txtName").value;
    var nameReg = /^([A-Za-z\s])+$/;
    var status = new Boolean();
        if(nameReg.test(name.trim())==false)
        {
            alert("Invalid name, don't put any number or symbol!");
            document.getElementById("txtName").value="";
            document.getElementById("txtName").style.background="#ff7e0e";
            status = false;
        }
        else
        {
            status = true;
            document.getElementById("txtName").style.background="#414141";
        }
    return status;
}

//function valPlate()
//{
//    var plateReg = /^(A-Z)+(0-9)$/;
//	var plate = document.getElementById("txtPlate").value;
//    var status = new Boolean();
//        if(plateReg.test(plate) == false)
//        {
//            alert("Invalid plate number!");
//            document.getElementById("txtPlate").value="";
//            document.getElementById("txtPlate").style.background="#ff7e0e";
//            status = false;
//        }
//        else
//        {
//            status = true;
//            document.getElementById("txtPlate").style.background="#414141";
//        }
//    return status;
//}


